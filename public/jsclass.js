/**
 * Created by kimberlykristen on 31/3/16.
 */

//GENERIC JS notes:
// strings can be in '' or "" in javascript only
// javascript sentence should always end in a semi colon ;



//declare variables: var xyz = "abc"
    //var = i'm defining a variable
    //xyz = i'm calling my variable xyz
    //value = i'm inputting a value that is a string, number, boolean or unknown


//eg create a variable called name, with the value being a string that is fred
var name = "fred";

//eg create a variable called age, with the value being a number that is 29
var age = 29;

//eg create a variable called married, with the value being a boolean
    //a boolean only ever has two value: true or false
var married = false;

//eg create a variable called address, with the value being an unknown
var address;


// will display variable name and input value
console.info("name = " + name);
console.info("age = " + age);
console.info("married = " + married);
console.info("address = " + address);


//IF ELSE statements
// less than <
// less than or equal to <=
// equal ==
// more than >
// more than or equal to =>
// not !
// not equal !=

//be careful not to use = as ==
// one = symbol is an assignment statement, that is,
// if age = 50, we are reassigning the value from 29 to 50 in the variable age
// two == is a comparison
// if age == 50, we are saying, if age is 50, run if. otherwise, run else.


// if variable (age) is more than input value (50)
if (age == 50) {
    console.log("Have you applied for your pioneer card yet?");
} else {
    console.log("You should give up your seat to the elderly.")
}





//WILD LOOPS
//create a variable and initialize it by typing var i = 0
//while i is less than 5
//i=i+1 = i++

var i = 0;
document.write("<ul>");
while (i < 5) {
    document.write("<li>Item " + i + "</li>");
    i = i+1
}
document.write("</ul>");



//FOUR LOOP
//i = 0 initialization (assignment of variable), i <5 condition (test), i++ increment
document.write("<ul>");
for (i = 0; i <5; i++) {
    document.write("<li>****Item " + i + "</li>");
}
document.write("</ul>");


// LOOP AN IMAGE

var sushiImg = "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSLQT1zo6qDOkgek_1XC8MSyuCJk0e67aue4b9M2_PI9B-xRUHwnA"
for (i = 0; i < 10; i++) {
    document.write("<img src='" + sushiImg + "'>");
}